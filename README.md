Weather
=======
A simple tool to query a weather API, get the current temperature and save it to
an influxdb database. 

Usage
=====
Set the following environment varliables: 

```
$ export WEATHERDB=<Your influxdb database name>
$ export WEATHERUSER=<Your influxdb user>
$ export WEATHERPASS=<Your influxdb pass>
$ export WEATHERAPIKEY=<Your wunderground API key>
```

Execute the program: 

```
$ weather
```

Currently, the program must be called each time you want to request the weather.
If you want to continuously poll, you'll need to use a scheduler like `cron(1)`
to repeatedly hit the weather endpoint.
