package main

import (
	"log"
	"log/syslog"
	"os"
	"os/signal"
	"syscall"
	"time"

	client "github.com/influxdata/influxdb/client/v2"
	"github.com/shawntoffel/darksky"
	"gitlab.com/techcoop/usage"
)

// Call weather API and get the current weather return weather object or an error
func getWeather(apiKey string, u *usage.Usage) (weather *darksky.ForecastResponse, err error) {
	// track usage w/in this loop
	u.Increment()
	client := darksky.New(apiKey)
	request := darksky.ForecastRequest{}
	request.Latitude = 45.096442
	request.Longitude = -72.811030
	request.Options = darksky.ForecastRequestOptions{Exclude: "hourly,minutely", Units: "ca"}

	forecast, err := client.Forecast(request)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	return &forecast, nil
}

// Save the current weather conditions
func saveWeather(weather *darksky.ForecastResponse, database string, db client.Client, u *usage.Usage) error {
	u.Increment()
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  database,
		Precision: "s",
	})
	if err != nil {
		log.Print(err)
		return err
	}

	// TODO - Add more metadata tags = location?
	fields := map[string]interface{}{
		"temp_c":      float64(weather.Currently.Temperature),
		"feelslike_c": float64(weather.Currently.ApparentTemperature),
	}

	pt, err := client.NewPoint(database, nil, fields, time.Now())
	if err != nil {
		log.Print(err)
		return err
	}
	bp.AddPoint(pt)

	if err := db.Write(bp); err != nil {
		log.Print(err)
		return err
	}
	return nil
}

func saveUsage(db client.Client, usageGet, usageSave *usage.Usage) error {
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  "usage",
		Precision: "us",
	})
	if err != nil {
		log.Print(err)
		return err
	}
	// TODO: what to save to influx db?
	// TODO: use tags for stuff that should be indexed: resource, userid.
	fields := map[string]interface{}{
		"userid":   usageGet.GetUserID(),
		"used":     usageGet.Get(),
		"resource": usageGet.GetResource(),
	}

	pt, err := client.NewPoint("usage", nil, fields, time.Now())
	if err != nil {
		log.Print(err)
		return err
	}
	bp.AddPoint(pt)

	fields = map[string]interface{}{
		"userid":   usageSave.GetUserID(),
		"used":     usageSave.Get(),
		"resource": usageSave.GetResource(),
	}

	pt, err = client.NewPoint("usage", nil, fields, time.Now())
	if err != nil {
		log.Print(err)
		return err
	}
	bp.AddPoint(pt)

	if err := db.Write(bp); err != nil {
		log.Print(err)
		return err
	}

	// Once usage is saved in influxdb, reset
	usageGet.Reset()
	usageSave.Reset()
	return nil
}

func main() {
	var database = os.Getenv("WEATHERDB")
	var username = os.Getenv("WEATHERUSER")
	var password = os.Getenv("WEATHERPASS")
	var apiKey = os.Getenv("DARKSKY")

	// Setup syslog
	sysLog, err := syslog.New(syslog.LOG_INFO|syslog.LOG_DAEMON, "weather")
	if err != nil {
		log.Fatal(err)
	}

	// Setup usage tracker for getWeather
	usageGet := usage.New("gabe", "getWeather")
	// Setup usage tracker for saveWeather
	usageSave := usage.New("gabe", "saveWeather")

	// Setup interrupt handler
	interruptChannel := make(chan os.Signal, 1)
	signal.Notify(interruptChannel, os.Interrupt, syscall.SIGTERM)
	go func() {
		for sig := range interruptChannel {
			// sig is a ^C, handle it
			log.Printf("caught signal %s\n", sig)
			log.Printf("shutting down...\n")
			log.Printf("%v", usageGet)
			log.Printf("%v", usageSave)
			sysLog.Info("shutting down...")
			sysLog.Info(usageGet.String())
			sysLog.Info(usageSave.String())
			os.Exit(1)
		}
	}()

	// Create a new HTTPClient connected to influxdb
	db, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     "http://localhost:8086",
		Username: username,
		Password: password,
	})
	if err != nil {
		sysLog.Info("could not connect to influxdb, shutting down...")
		log.Fatal(err)
	}

	c := time.Tick(2 * time.Minute)
	sysLog.Info("entering main loop...")
	for range c {
		weather, err := getWeather(apiKey, usageGet)
		if err != nil {
			log.Println(err)
			continue
		}
		err = saveWeather(weather, database, db, usageSave)
		if err != nil {
			log.Println(err)
			continue
		}

		saveUsage(db, usageGet, usageSave)
	}
}
